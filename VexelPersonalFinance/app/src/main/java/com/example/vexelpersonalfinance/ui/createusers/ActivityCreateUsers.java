package com.example.vexelpersonalfinance.ui.createusers;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.vexelpersonalfinance.R;
import com.example.vexelpersonalfinance.data.database.PersonalFinanceDatabase;
import com.example.vexelpersonalfinance.data.models.Person;
import com.example.vexelpersonalfinance.tools.Constants;
import com.example.vexelpersonalfinance.tools.CreateImageFile;
import com.example.vexelpersonalfinance.tools.Functions;
import com.example.vexelpersonalfinance.tools.ViewModelFactory;
import com.example.vexelpersonalfinance.tools.ViewState;
import com.example.vexelpersonalfinance.ui.main.MainActivity;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

public class ActivityCreateUsers extends AppCompatActivity {

    private SharedPreferences sharedPreferences = null;

    private Button btCreateProfile;
    private EditText etFirstUserName, etSecondUserName;
    private ImageButton ibProfilePickerFirst, ibProfilePickerSecond;

    private ActivityCreateUsersViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        setContentView(R.layout.activity_create_users);
        getViewReferences();
        checkAppPermissions();

        viewModel = ViewModelProviders.of(this, new ViewModelFactory(PersonalFinanceDatabase.Companion.getDatabase(this))).get(ActivityCreateUsersViewModel.class);

        setControls();
    }

    private void getViewReferences() {
        ibProfilePickerFirst = findViewById(R.id.ibProfilePickerFirst);
        ibProfilePickerSecond = findViewById(R.id.ibProfilePickerSecond);
        etFirstUserName = findViewById(R.id.etFirstUserName);
        etSecondUserName = findViewById(R.id.etSecondUserName);
        btCreateProfile = findViewById(R.id.btCreateProfile);
    }

    /* Checks permission using Dexter Permission library */
    public void checkAppPermissions() {
        if (getWindow() == null) return;
        View currentView = getWindow().getDecorView().getRootView();
        Functions.checkAppPermissions(currentView, ActivityCreateUsers.this,
                sharedPreferences, result -> {
                    if (result.equals(true))
                        observe();
                    else {
                        Functions.createSnackBar(currentView, Constants.PERMISSION_DENIED);
                        final Handler handler = new Handler();
                        handler.postDelayed(this::checkAppPermissions, 500);
                    }
                });
    }

    private void observe() {
        viewModel.getAllPeople().observe(this, people -> {
            checkDatabase(people.size());
        });
    }

    /* Checks if the database table for users has exactly 2 user and exactly 2 profile images
    configured. If not, configuration is called! */
    private void checkDatabase(Integer peopleCount) {
        Log.d("DEBUG", String.valueOf(peopleCount));
        if (peopleCount == 2 &&
                Functions.checkIfFileExists(Constants.FIRST_USER_FILE_NAME) &&
                Functions.checkIfFileExists(Constants.SECOND_USER_FILE_NAME)) {
            goToNextScreen();
            return;
        }
        clearDatabaseAndImageFiles();
    }

    private void goToNextScreen() {
        Intent intent = new Intent(ActivityCreateUsers.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /* If the app needs reconfiguration, the image files saved in local storage (if they do exist)
    are wiped for a clean image configuration */
    private void clearDatabaseAndImageFiles() {
        viewModel.deleteAllExpenses();
        viewModel.deleteAllPeople();
        File file1 = new File(Constants.FIRST_USER_FULL_PATH);
        File file2 = new File(Constants.SECOND_USER_FULL_PATH);
        file1.delete();
        file2.delete();
    }

    private void setControls() {
        ibProfilePickerFirst.setOnClickListener(v ->
                startProfilePicturePicker(Constants.REQUEST_CODE_USER_1));
        ibProfilePickerSecond.setOnClickListener(v ->
                startProfilePicturePicker(Constants.REQUEST_CODE_USER_2));
        btCreateProfile.setOnClickListener(v -> createProfile());
    }

    private void startProfilePicturePicker(int requestCode) {
        Intent cropImageIntent = CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .getIntent(this);
        startActivityForResult(cropImageIntent, requestCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String profilePictureName, profilePicturePath;

        if (requestCode == Constants.REQUEST_CODE_USER_1) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                profilePictureName = Constants.FIRST_USER_FILE_NAME;
                profilePicturePath = Environment.getExternalStorageDirectory().toString()
                        + Constants.IMAGE_STORAGE_FOLDER + profilePictureName;
                assert result != null;

                CreateImageFile.createImage(ibProfilePickerFirst, result, profilePictureName, profilePicturePath, this);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
                if (result != null) logError(result);

        } else if (requestCode == Constants.REQUEST_CODE_USER_2) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {

                profilePictureName = Constants.SECOND_USER_FILE_NAME;
                profilePicturePath = Environment.getExternalStorageDirectory().toString()
                        + Constants.IMAGE_STORAGE_FOLDER + profilePictureName;

                assert result != null;
                CreateImageFile.createImage(ibProfilePickerSecond, result, profilePictureName, profilePicturePath, this);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
                if (result != null) logError(result);

        } else Log.d("RESULT_CODE_EXCEPTION", "BAD RESULT_CODE");
    }

    public void logError(@NonNull CropImage.ActivityResult result) {
        Exception error = result.getError();
        Log.d("IMAGE_SETUP", "ERROR: Crop Activity - " + error.toString());
    }

    private void createProfile() {
        if (ViewState.checkEmptyET(ActivityCreateUsers.this,
                etFirstUserName, Constants.PROFILE_EMPTY_NAMES) &&
                ViewState.checkEmptyET(ActivityCreateUsers.this,
                        etSecondUserName, Constants.PROFILE_EMPTY_NAMES)) {
            if (Functions.checkIfFileExists(Constants.FIRST_USER_FILE_NAME) &&
                    Functions.checkIfFileExists(Constants.SECOND_USER_FILE_NAME)) {

                createPerson(1, etFirstUserName.getText().toString(), Constants.FIRST_USER_FULL_PATH);
                createPerson(2, etSecondUserName.getText().toString(), Constants.SECOND_USER_FULL_PATH);

                Intent intent = new Intent(ActivityCreateUsers.this, MainActivity.class);
                startActivity(intent);
                finish();
                return;
            }
            Functions.createToast(ActivityCreateUsers.this, Constants.PROFILE_NO_PICTURES);
        }
    }

    public void createPerson(int personID, String personName, String personImagePath) {
        Person person = new Person(personID, personName, personImagePath);
        viewModel.insertPerson(person);
        Log.d("CHECK_DB_OPERATION", personID + " "
                + personName + " " + personImagePath + "; ");
    }
}