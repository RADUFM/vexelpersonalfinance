package com.example.vexelpersonalfinance.tools;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.vexelpersonalfinance.data.database.PersonalFinanceDatabase;
import com.example.vexelpersonalfinance.data.repositories.ExpenseRepository;
import com.example.vexelpersonalfinance.data.repositories.PeopleRepository;
import com.example.vexelpersonalfinance.ui.createusers.ActivityCreateUsersViewModel;
import com.example.vexelpersonalfinance.ui.expensedialog.ExpenseDialogViewModel;
import com.example.vexelpersonalfinance.ui.main.MainActivityViewModel;

import org.apache.commons.lang3.NotImplementedException;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private PersonalFinanceDatabase database;

    public ViewModelFactory(PersonalFinanceDatabase database) {
        this.database = database;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (database == null) {
            throw new IllegalArgumentException("Database is null!!");
        }

        if (modelClass.isAssignableFrom(MainActivityViewModel.class)) {
            return (T) new MainActivityViewModel(new ExpenseRepository(database.expenseDao()));
        }

        if (modelClass.isAssignableFrom(ExpenseDialogViewModel.class)) {
            return (T) new ExpenseDialogViewModel(new PeopleRepository(database.peopleDao()));
        }

        if (modelClass.isAssignableFrom(ActivityCreateUsersViewModel.class)) {
            return (T) new ActivityCreateUsersViewModel(new PeopleRepository(database.peopleDao()),
                    new ExpenseRepository(database.expenseDao()));
        }

        throw new NotImplementedException("ViewModel not known for ViewModelFactory");
    }
}