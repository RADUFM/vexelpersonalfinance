package com.example.vexelpersonalfinance.ui.expensedialog;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vexelpersonalfinance.R;
import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IPersonAdapterCallback;
import com.example.vexelpersonalfinance.data.models.Person;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.MyViewHolder> {

    private final IPersonAdapterCallback callback;
    private List<Person> items = new ArrayList<>();
    private int selectedPersonID = -1;

    PersonAdapter(IPersonAdapterCallback callback) {
        this.callback = callback;
    }

    void resetList(List<Person> personList) {
        this.items = personList;
        notifyDataSetChanged();
    }

    void selectPerson(int selectedPersonID){
        this.selectedPersonID = selectedPersonID;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_person, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final PersonAdapter.MyViewHolder holder, final int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ibPersonAvatar;

        MyViewHolder(View view) {
            super(view);
            getViewHolderReferences(view);
        }

        private void getViewHolderReferences(View view) {
            ibPersonAvatar = view.findViewById(R.id.ibPersonAvatar);
        }

        void bind(Person person) {
            setControls(person);
            setPersonAvatar(person);
            checkSelect(person);
        }

        private void setPersonAvatar(Person person) {
            Glide.with(itemView.getContext())
                    .load(Uri.fromFile(new File(person.getProfilePath())))
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(ibPersonAvatar);
        }

        private void checkSelect(Person person) {
            Log.d("IMPORTANT", person.getPersonLocalID() + " " + selectedPersonID);
            if(person.getPersonLocalID() == selectedPersonID){
                callback.onPersonSelected(person);
                ibPersonAvatar.setAlpha(1.0f);
                return;
            }
            ibPersonAvatar.setAlpha(0.4f);
        }

        /* selectedPersonID is a variable that holds the ID key of a selected person item.
        * Initially, this variable is -1 (nothing yet selected), but if the user clicks on any
        * item in the recycleView, in the function bellow, the variable becomes the id of the
        * person that was selected and a function above uses this id to change the opacity of the
        * selected item. In the mean time, the entire Person object is sent through an interface
        * to the main activity so it can server further purposes. If the user chooses to update an
        * existing item, the selected item ID gets passed in here as a parameter of the constructor
        * and the item gets automatically selected.*/

        private void setControls(Person person) {
            ibPersonAvatar.setOnClickListener(v -> {
                selectedPersonID = person.getPersonLocalID();
                callback.onPersonSelected(person);
                notifyDataSetChanged();
            });
        }
    }
}