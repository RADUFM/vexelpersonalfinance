package com.example.vexelpersonalfinance.ui.uicomponents.interfaces;

import com.example.vexelpersonalfinance.data.models.Person;

public interface IPersonAdapterCallback {

    void onPersonSelected(Person person);

}
