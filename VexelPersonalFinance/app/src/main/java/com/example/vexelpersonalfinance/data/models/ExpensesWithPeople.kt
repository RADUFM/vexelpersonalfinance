package com.example.vexelpersonalfinance.data.models

import androidx.room.Embedded
import androidx.room.Relation

class ExpensesWithPeople {

    @Embedded
    var expense: Expense? = null

    @Relation(parentColumn = "personID", entityColumn = "personLocalID", entity = Person::class)
    var people: List<Person>? = null
}
