package com.example.vexelpersonalfinance.ui.expensedialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.vexelpersonalfinance.R;
import com.example.vexelpersonalfinance.data.database.PersonalFinanceDatabase;
import com.example.vexelpersonalfinance.data.models.Expense;
import com.example.vexelpersonalfinance.data.models.ExpenseType;
import com.example.vexelpersonalfinance.data.models.Person;
import com.example.vexelpersonalfinance.tools.Constants;
import com.example.vexelpersonalfinance.tools.ViewModelFactory;
import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IDialogCallbacks;
import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IExpenseTypeAdapterCallback;
import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IPersonAdapterCallback;
import java.util.Objects;

public class ExpenseDialogFragment extends DialogFragment implements IExpenseTypeAdapterCallback, IPersonAdapterCallback {

    private ExpenseDialogViewModel viewModel;

    private IDialogCallbacks iDialogCallbacks;

    private ImageButton ibDismiss;
    private Button btCreateUpdate;
    private PersonAdapter personAdapter;
    private RecyclerView rvExpenseType, rvPerson;
    private EditText etExpenseAmount, etExpenseDescription;
    private ExpenseTypeAdapter expenseTypeAdapter;

    /* Instance for a bundle passage that is used by the main activity to send an Expense
     * object to this fragment when item update button is pressed */
    public static ExpenseDialogFragment newInstance(Expense expense) {
        ExpenseDialogFragment fragment = new ExpenseDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(Constants.BUNDLE_EXPENSE_KEY, expense);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragment);
        setCancelable(false);
        if (getDialog() == null) return;
        if (getDialog().getWindow() == null) return;
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_expense,
                container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (!(context instanceof Activity)) {
            return;
        }
        Activity activity = (Activity) context;
        try {
            this.iDialogCallbacks = (IDialogCallbacks) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        WindowManager.LayoutParams layoutParams = Objects.requireNonNull(
                Objects.requireNonNull(getDialog()).getWindow()).getAttributes();
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        Objects.requireNonNull(getDialog().getWindow()).setAttributes(layoutParams);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewReferences(view);

        viewModel = ViewModelProviders.of(this, new ViewModelFactory(
                PersonalFinanceDatabase.Companion.getDatabase(view.getContext())))
                .get(ExpenseDialogViewModel.class);

        configView();
        setViewModelArgs();
        observe();
        setControls();
    }

    @Override
    public void onExpenseTypeSelected(ExpenseType expenseType) {
        viewModel.setSelectedExpenseType(expenseType);
    }

    @Override
    public void onPersonSelected(Person person) {
        viewModel.setSelectedPerson(person);
    }

    private void getViewReferences(View view) {
        rvExpenseType = view.findViewById(R.id.rvExpenseType);
        rvPerson = view.findViewById(R.id.rvPerson);
        etExpenseAmount = view.findViewById(R.id.etExpenseAmount);
        etExpenseDescription = view.findViewById(R.id.etExpenseDescription);
        btCreateUpdate = view.findViewById(R.id.btCreateUpdate);
        ibDismiss = view.findViewById(R.id.ibDismiss);
    }

    private void configView() {
        //rv expenses:
        rvExpenseType.setLayoutManager(new GridLayoutManager(getContext(), 2));
        expenseTypeAdapter = new ExpenseTypeAdapter(this);
        expenseTypeAdapter.setList(viewModel.expenseTypes);
        rvExpenseType.setAdapter(expenseTypeAdapter);
        rvExpenseType.setNestedScrollingEnabled(false);

        //rv person
        rvPerson.setLayoutManager(new GridLayoutManager(getContext(), 2));
        personAdapter = new PersonAdapter(this);
        rvPerson.setAdapter(personAdapter);
        rvPerson.setNestedScrollingEnabled(false);
    }

    /* Gets the data from the bundle. If there is no data sent (so the fragment should create
     * an item and not update), the local expense object will remain null. */
    private void setViewModelArgs() {
        assert getArguments() != null;
        viewModel.setExpense(getArguments().getParcelable(Constants.BUNDLE_EXPENSE_KEY));
    }

    private void observe() {
        viewModel.getAllPeople().observe(this, personList ->
                personAdapter.resetList(personList));

        viewModel.getExpense().observe(this, expense -> {
            personAdapter.selectPerson(expense != null ? expense.getPersonID() : -1);
            expenseTypeAdapter.selectExpenseType(expense != null ? expense.getExpenseType() : null);
            if (expense != null) {
                etExpenseAmount.setText(String.valueOf(expense.getAmount()));
                etExpenseDescription.setText(expense.getDescription());
            }
        });

        viewModel.getDialogType().observe(this, dialogType -> {
            switch (dialogType) {
                case UPDATE:
                    btCreateUpdate.setText(R.string.UPDATE_BUTTON);
                    break;
                case CREATE:
                    btCreateUpdate.setText(R.string.CREATE_BUTTON);
                    break;
            }
        });
    }

    private void setControls() {
        ibDismiss.setOnClickListener(v -> dismiss());
        btCreateUpdate.setOnClickListener(v -> onFinishButtonPressed());
        etExpenseDescription.setOnFocusChangeListener((view, isFocused) -> {
            if (isFocused) etExpenseDescription.setHint("");
            else etExpenseDescription.setHint(R.string.dfCreateExpDescriptionHint);
        });
    }

    private void onFinishButtonPressed() {
        if (viewModel.getExpense().getValue() == null) {
            this.iDialogCallbacks.onDialogCreateExpense(viewModel.createExpense(
                    etExpenseAmount.getText().toString(),
                    etExpenseDescription.getText().toString())
            );
            dismiss();
            return;
        }
//        this.iDialogCallbacks.onDialogUpdateExpense(viewModel.getExpense().getValue());
        this.iDialogCallbacks.onDialogUpdateExpense(viewModel.updateExpense(
                etExpenseAmount.getText().toString(),
                etExpenseDescription.getText().toString())
        );
        dismiss();
    }
}
