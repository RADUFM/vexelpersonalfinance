package com.example.vexelpersonalfinance.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

import com.example.vexelpersonalfinance.data.dao.ExpenseDao
import com.example.vexelpersonalfinance.data.dao.PeopleDao
import com.example.vexelpersonalfinance.data.models.Expense
import com.example.vexelpersonalfinance.data.models.Person

@Database(entities = [Expense::class, Person::class], version = 13, exportSchema = false)
abstract class PersonalFinanceDatabase : RoomDatabase() {

    abstract fun expenseDao(): ExpenseDao
    abstract fun peopleDao(): PeopleDao

    companion object {
        @Volatile
        private var wordRoomDatabaseInstance: PersonalFinanceDatabase? = null
        private val LOCK = Any()

        fun getDatabase(context: Context) =
                wordRoomDatabaseInstance ?: synchronized(LOCK){
                    wordRoomDatabaseInstance ?: Room.databaseBuilder(context.applicationContext,
                            PersonalFinanceDatabase::class.java, "PERSONAL_FINANCE_DATABASE")
                            .fallbackToDestructiveMigration()
                            .build()
                }
    }
}

