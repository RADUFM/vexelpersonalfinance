package com.example.vexelpersonalfinance.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.vexelpersonalfinance.R;

public class CustomAlertDialog extends AlertDialog implements View.OnClickListener {

    private Activity activity;
    private Button btAccept, btDeny;
    private TextView tvCustomAlertTitle;
    private String alertTitle;

    CustomAlertDialog(Activity activity, String alertTitle) {
        super(activity);
        this.alertTitle = alertTitle;
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.custom_alert_dialog);
        getViewReferences();
        setControls();
        setTitleText();
    }

    private void setControls() {
        btAccept.setOnClickListener(this);
        btDeny.setOnClickListener(this);
    }

    private void setTitleText() {
        tvCustomAlertTitle.setText(alertTitle);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btAccept:
                activity.finish();
                break;
            case R.id.btDeny:
                break;
            default:
                break;
        }
    }

    private void getViewReferences() {
        btAccept = findViewById(R.id.btAccept);
        btDeny = findViewById(R.id.btDeny);
        tvCustomAlertTitle = findViewById(R.id.tvCustomAlertTitle);
    }
}
