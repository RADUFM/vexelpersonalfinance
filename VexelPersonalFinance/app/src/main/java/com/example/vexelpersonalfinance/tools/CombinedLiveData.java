package com.example.vexelpersonalfinance.tools;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

public class CombinedLiveData<A, B> extends MediatorLiveData<CombinedValue<A, B>> {

    private A first;
    private B second;

    public CombinedLiveData(LiveData<A> sourceA, LiveData<B> sourceB) {
        addSource(sourceA, a -> {
            first = a;
            postNonNull(first, second);
        });
        addSource(sourceB, b -> {
            second = b;
            postNonNull(first, second);
        });
    }
    @SuppressWarnings("unchecked")
    private void postNonNull(A first, B second) {
        postValue(new CombinedValue(first, second));
    }
}