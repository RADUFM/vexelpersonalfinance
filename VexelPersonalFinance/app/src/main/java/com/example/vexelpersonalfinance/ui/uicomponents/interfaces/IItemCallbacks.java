package com.example.vexelpersonalfinance.ui.uicomponents.interfaces;

import com.example.vexelpersonalfinance.data.models.Expense;

public interface IItemCallbacks {

    void onItemEdit(Expense expense);
    void onItemDelete(Expense expense);
    void onItemPayment(Expense expense);

}
