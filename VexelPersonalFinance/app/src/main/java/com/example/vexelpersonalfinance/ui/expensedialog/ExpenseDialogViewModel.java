package com.example.vexelpersonalfinance.ui.expensedialog;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.vexelpersonalfinance.data.models.Expense;
import com.example.vexelpersonalfinance.data.models.ExpenseType;
import com.example.vexelpersonalfinance.data.models.Person;
import com.example.vexelpersonalfinance.data.repositories.PeopleRepository;
import com.example.vexelpersonalfinance.tools.Functions;
import java.util.Arrays;
import java.util.List;

public class ExpenseDialogViewModel extends ViewModel {

    enum DialogType {
        UPDATE,
        CREATE
    }

    final List<ExpenseType> expenseTypes = Arrays.asList(ExpenseType.values());

    private PeopleRepository peopleRepository;

    private MutableLiveData<Person> selectedPerson = new MutableLiveData<>();

    private MutableLiveData<Expense> expense = new MutableLiveData<>();

    private MutableLiveData<ExpenseType> selectedExpenseType = new MutableLiveData<>();

    private MutableLiveData<DialogType> dialogType = new MutableLiveData<>();

    public ExpenseDialogViewModel(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    @SuppressWarnings("ConstantConditions")
    @Nullable
    Expense createExpense(String amount, String description) {
        if (!isValid()) {
            return null;
        }
        return new Expense(
                1,
                selectedExpenseType.getValue().getText(),
                selectedPerson.getValue().getPersonLocalID(),
                Double.valueOf(amount),
                description,
                Functions.getCurrentTime(),
                0);
    }

    @SuppressWarnings("ConstantConditions")
    @Nullable
    Expense updateExpense(String amount, String description) {
        if (!isValid()) {
            return null;
        }
        return new Expense(
                expense.getValue().getExpenseID(),
                selectedExpenseType.getValue().getText(),
                selectedPerson.getValue().getPersonLocalID(),
                Double.valueOf(amount),
                description,
                Functions.getCurrentTime(),
                0);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isValid() {
        return selectedPerson.getValue() != null && selectedExpenseType.getValue() != null;
    }

    LiveData<List<Person>> getAllPeople() {
        return peopleRepository.getAllPeople();
    }

    void setSelectedPerson(Person person) {
        selectedPerson.postValue(person);
    }

    void setSelectedExpenseType(ExpenseType expenseType) {
        selectedExpenseType.postValue(expenseType);
    }

    void setExpense(Expense expense) {
        this.dialogType.postValue(expense == null ? DialogType.CREATE : DialogType.UPDATE);
        this.expense.postValue(expense);
    }

    MutableLiveData<Expense> getExpense() {
        return expense;
    }

    MutableLiveData<DialogType> getDialogType() {
        return dialogType;
    }
}
