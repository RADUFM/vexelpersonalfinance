package com.example.vexelpersonalfinance.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageButton;
import com.theartofdev.edmodo.cropper.CropImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

import id.zelory.compressor.Compressor;

public class CreateImageFile {

    private static Bitmap compressedThumbnail;

    /* Using the result of the CropImage activity, this function compresses the chosen image
    * file to a custom size and quality. Then it uses a custom path and file name to create
    * the picture to the user's custom location. If this function is called from a reconfiguration
    * of the user profile, the function also checks if the folder exists (if not, the folder gets
    * created) and checks if there are image files from an older configuration (if they are, they
    * are automatically wiped). The function then saves the images to the custom location and sets
    * them as image resources for the imagePicker button. */

    public static void createImage(ImageButton imageButton, CropImage.ActivityResult result,
                                   String profilePictureName, String profilePicturePath,
                                   Context context){

        assert result != null;
        File thumbnailURI = new File(Objects.requireNonNull(result.getUri().getPath()));
        try {
            compressedThumbnail = new Compressor(context)
                    .setMaxHeight(200)
                    .setMaxWidth(200)
                    .setQuality(90)
                    .compressToBitmap(thumbnailURI);
        } catch (
                IOException e) {
            e.printStackTrace();
            Log.d("Compression_FAILURE", e.toString());
        }
        File imagePath = new File(profilePicturePath);
        FileOutputStream fos;
        try {
            if (!imagePath.exists()) {
                File imageDir = new File(Environment.getExternalStorageDirectory().toString()
                        + Constants.IMAGE_STORAGE_FOLDER);
                if (!imageDir.exists()) {
                    if (imageDir.mkdirs()) {
                        Log.d("IMAGE_SETUP", "Created New Folder!");
                        return;
                    }
                    Log.d("IMAGE_SETUP", "Folder already exists!");
                    return;
                }
                Log.d("IMAGE_SETUP", "Folder Does Not Exist");
                if (Functions.checkIfFileExists(profilePictureName)) {
                    if (Functions.getImageFile(profilePictureName).delete()) {
                        Log.d("IMAGE_SETUP", "File found and deleted for replacement!");
                        return;
                    }
                    Log.d("IMAGE_SETUP", "Cannot Delete Files!");
                    return;
                }
                Log.d("IMAGE_SETUP", "Files Do Not Exist!");
            }

            fos = new FileOutputStream(imagePath);
            compressedThumbnail.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (
                FileNotFoundException e) {
            Log.d("IMAGE_SETUP", "EXCEPTION: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageButton.setImageBitmap(compressedThumbnail);
    }
}