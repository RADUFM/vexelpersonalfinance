package com.example.vexelpersonalfinance.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.vexelpersonalfinance.tools.Constants

@Entity(tableName = Constants.PEOPLE_TABLE_NAME)
class Person constructor(
        @PrimaryKey
        @ColumnInfo(name = "personLocalID")
        var personLocalID: Int,
        @ColumnInfo(name = "personName")
        var personName: String,
        @ColumnInfo(name = "profilePath")
        var profilePath: String)
