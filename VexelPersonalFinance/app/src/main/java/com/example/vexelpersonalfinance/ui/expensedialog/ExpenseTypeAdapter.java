package com.example.vexelpersonalfinance.ui.expensedialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.example.vexelpersonalfinance.R;
import com.example.vexelpersonalfinance.data.models.ExpenseType;
import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IExpenseTypeAdapterCallback;

import java.util.List;

public class ExpenseTypeAdapter extends RecyclerView.Adapter<ExpenseTypeAdapter.MyViewHolder> {

    private final IExpenseTypeAdapterCallback callback;
    private List<ExpenseType> expenseTypeList;
    private String selectedExpenseType = null;

    public ExpenseTypeAdapter(IExpenseTypeAdapterCallback callback) {
        this.callback = callback;
    }

    public void setList(List<ExpenseType> expenseTypeList){
        this.expenseTypeList = expenseTypeList;
    }

    public void selectExpenseType(String selectedExpenseType){
        this.selectedExpenseType = selectedExpenseType;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_expense_type, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ExpenseTypeAdapter.MyViewHolder holder,
                                 final int position) {
        holder.bind(expenseTypeList.get(position));
    }

    @Override
    public int getItemCount() {
        return expenseTypeList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout gridContainer;
        private TextView gridText;
        private ImageView gridIcon;

        MyViewHolder(View view) {
            super(view);
            getViewHolderReferences(view);
        }

        private void getViewHolderReferences(View view) {
            gridContainer = view.findViewById(R.id.grid_container);
            gridText = view.findViewById(R.id.grid_text);
            gridIcon = view.findViewById(R.id.grid_icon);
        }

        void bind(ExpenseType expenseType) {
            setData(expenseType);
            setControls(expenseType);
            checkSelect(expenseType);
        }

        private void checkSelect(ExpenseType expenseType) {
            if(expenseType.getText().equals(selectedExpenseType)){
                callback.onExpenseTypeSelected(expenseType);
                gridText.setTextColor(ContextCompat.getColor(itemView.getContext(),
                        android.R.color.white));
                gridIcon.setImageResource(expenseType.getDrawableWhite());
                gridContainer.setBackgroundResource(R.drawable.grid_blue_background);
                return;
            }
            gridText.setTextColor(ContextCompat.getColor(itemView.getContext(),
                    R.color.darkGreyBlue));
            gridIcon.setImageResource(expenseType.getDrawableBlue());
            gridContainer.setBackgroundResource(R.drawable.grid_white_background);
        }

        private void setData(ExpenseType expenseType) {
            gridText.setText(expenseType.getText());
        }

        /* See PersonAdapter.class for an explanation that applies to this adapter as well */
        private void setControls(ExpenseType expenseType) {
            itemView.setOnClickListener(v -> {
                selectedExpenseType = expenseType.getText();
                callback.onExpenseTypeSelected(expenseType);
                notifyDataSetChanged();
            });
        }
    }
}