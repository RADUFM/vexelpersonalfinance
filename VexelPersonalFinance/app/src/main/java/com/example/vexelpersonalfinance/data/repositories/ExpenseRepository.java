package com.example.vexelpersonalfinance.data.repositories;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.example.vexelpersonalfinance.data.dao.ExpenseDao;
import com.example.vexelpersonalfinance.data.models.Expense;
import com.example.vexelpersonalfinance.data.models.ExpensesWithPeople;
import com.example.vexelpersonalfinance.tools.CombinedLiveData;

import java.util.List;

public class ExpenseRepository {

    private ExpenseDao expenseDao;

    public ExpenseRepository(ExpenseDao expenseDao) {
        this.expenseDao = expenseDao;
    }

    public LiveData<List<ExpensesWithPeople>> getAllExpensesWithPeople() {
        return expenseDao.getAllExpensesWithPeople();
    }

    public void insertExpense(Expense expense) {
        new insertExpenseAsync(expenseDao).execute(expense);
    }

    public void updateExpense(Expense expense) {
        new UpdateExpenseAsync(expenseDao).execute(expense);
    }

    public void deleteExpense(Expense expense) {
        new deleteExpenseAsync(expenseDao).execute(expense);
    }

    public LiveData<Double> getUserDebt() {

        CombinedLiveData<Double, Double> combined = new CombinedLiveData<>(
                expenseDao.getFirstUserSum(),
                expenseDao.getSecondUserSum()
        );
        return Transformations.map(combined, input -> {
            Double first = input.getFirst()!=null? input.getFirst(): 0.0;
            Double second = input.getSecond()!=null? input.getSecond(): 0.0;
            return first - second;
        });
    }

    public void payAllExpenses() {
        new PayAllExpensesAsync(expenseDao).execute();
    }

    public void deleteAllExpenses() {
        new deleteAllExpensesAsync(expenseDao).execute();
    }

    private static class insertExpenseAsync extends AsyncTask<Expense, Void, Void> {
        private ExpenseDao personalFinanceDao;

        insertExpenseAsync(ExpenseDao dao) {
            personalFinanceDao = dao;
        }

        @Override
        protected Void doInBackground(final Expense... params) {
            personalFinanceDao.insertExpense(params[0]);
            return null;
        }
    }

    private static class UpdateExpenseAsync extends AsyncTask<Expense, Void, Void> {
        private ExpenseDao personalFinanceDao;

        UpdateExpenseAsync(ExpenseDao dao) {
            personalFinanceDao = dao;
        }

        @Override
        protected Void doInBackground(final Expense... params) {
            personalFinanceDao.updateExpense(params[0]);
            return null;
        }
    }

    private static class deleteExpenseAsync extends AsyncTask<Expense, Void, Void> {
        private ExpenseDao personalFinanceDao;

        deleteExpenseAsync(ExpenseDao dao) {
            personalFinanceDao = dao;
        }

        @Override
        protected Void doInBackground(final Expense... params) {
            personalFinanceDao.deleteExpense(params[0]);
            return null;
        }
    }

    private static class deleteAllExpensesAsync extends AsyncTask<Void, Void, Void> {
        private ExpenseDao personalFinanceDao;

        deleteAllExpensesAsync(ExpenseDao dao) {
            personalFinanceDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            personalFinanceDao.deleteAllExpenses();
            return null;
        }
    }

    private static class PayAllExpensesAsync extends AsyncTask<Void, Void, Void> {
        private ExpenseDao personalFinanceDao;

        PayAllExpensesAsync(ExpenseDao dao) {
            personalFinanceDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            personalFinanceDao.payAllExpenses();
            return null;
        }
    }
}

