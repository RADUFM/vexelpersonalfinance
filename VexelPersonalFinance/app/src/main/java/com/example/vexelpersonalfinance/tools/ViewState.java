package com.example.vexelpersonalfinance.tools;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;

public class ViewState {

    public static boolean checkEmptyET(Context context, EditText editText, String errorText) {
        if (!TextUtils.isEmpty(editText.getText().toString()))
            return true;
        Functions.createToast(context, errorText);
        return false;
    }
}
