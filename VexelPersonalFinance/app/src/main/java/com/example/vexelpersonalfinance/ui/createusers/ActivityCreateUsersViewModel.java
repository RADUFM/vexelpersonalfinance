package com.example.vexelpersonalfinance.ui.createusers;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.example.vexelpersonalfinance.data.models.Person;
import com.example.vexelpersonalfinance.data.repositories.ExpenseRepository;
import com.example.vexelpersonalfinance.data.repositories.PeopleRepository;
import java.util.List;

public class ActivityCreateUsersViewModel extends ViewModel {

    private PeopleRepository peopleRepository;
    private ExpenseRepository expenseRepository;

    public ActivityCreateUsersViewModel(PeopleRepository peopleRepository,
                                        ExpenseRepository expenseRepository) {
        this.peopleRepository = peopleRepository;
        this.expenseRepository = expenseRepository;
    }

    LiveData<List<Person>> getAllPeople() {
        return peopleRepository.getAllPeople();
    }

    void insertPerson(Person person) {
        peopleRepository.insertPerson(person);
    }

    void deleteAllExpenses() {
        expenseRepository.deleteAllExpenses();
    }

    void deleteAllPeople() {
        peopleRepository.deleteAllPeople();
    }
}
