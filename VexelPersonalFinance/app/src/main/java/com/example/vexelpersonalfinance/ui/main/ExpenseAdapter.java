package com.example.vexelpersonalfinance.ui.main;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vexelpersonalfinance.R;
import com.example.vexelpersonalfinance.data.models.Expense;
import com.example.vexelpersonalfinance.data.models.ExpenseType;
import com.example.vexelpersonalfinance.data.models.Person;
import com.example.vexelpersonalfinance.tools.Constants;
import com.example.vexelpersonalfinance.tools.Functions;
import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IItemCallbacks;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.MyViewHolder> {

    private final IItemCallbacks callback;
    private List<Expense> expenses = new ArrayList<>();

    ExpenseAdapter(IItemCallbacks callback) {
        this.callback = callback;
        setHasStableIds(true);
    }

    public void setData(List<Expense> expenses) {
        this.expenses = expenses;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_expense, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.bind(expenses.get(position));
    }

    @Override
    public int getItemCount() {
        return expenses.size();
    }

    @Override
    public long getItemId(int position) {
        return expenses.get(position).getExpenseType().hashCode();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivDebtType, ivPersonThatOwes, ivPersonThatReceives;
        private TextView tvAmount, tvDescription, tvTimestamp;
        private ImageButton ibDelete, ibEdit, ibPay;
        private LinearLayout llItemContainer;

        MyViewHolder(View view) {
            super(view);
            getViewHolderReferences(view);
        }

        private void getViewHolderReferences(View view) {
            llItemContainer = view.findViewById(R.id.llItemContainer);
            ivDebtType = view.findViewById(R.id.ivDebtType);
            ivPersonThatOwes = view.findViewById(R.id.ivPersonThatOwes);
            ivPersonThatReceives = view.findViewById(R.id.ivPersonThatReceives);
            tvAmount = view.findViewById(R.id.tvAmount);
            ibDelete = view.findViewById(R.id.ibDelete);
            ibEdit = view.findViewById(R.id.ibEdit);
            ibPay = view.findViewById(R.id.ibPay);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvTimestamp = view.findViewById(R.id.tvTimestamp);
        }

        void bind(Expense expense) {
            displayPaidState(expense);
            setData(expense);
            setActions(expense);
        }



        private void displayPaidState(Expense expense) {
            if (expense.isPaid() == 1) {
                llItemContainer.setAlpha(0.3f);
                ibPay.setVisibility(View.GONE);
                ibEdit.setVisibility(View.GONE);
                return;
            }
            llItemContainer.setAlpha(1f);
            ibPay.setVisibility(View.VISIBLE);
            ibEdit.setVisibility(View.VISIBLE);
        }

        private void setData(Expense expense) {
            ivDebtType.setImageResource(Objects.requireNonNull(
                    ExpenseType.Companion.getExpenseTypeForName(expense.getExpenseType())).getDrawableBlue());
            setPersonPictureWithGlide(expense);
            tvAmount.setText(String.valueOf(expense.getAmount()));
            tvDescription.setText(expense.getDescription());
            long currentTime = Long.parseLong(expense.getTimestamp());
            tvTimestamp.setText(Functions.formatDate(currentTime, Constants.FORMAT_YYY_MMM_d_HH_mm));
        }

        /* Changes the images in relation to who owes who. */
        private void setPersonPictureWithGlide(Expense expense) {
            String pathUser1 = Constants.FIRST_USER_FULL_PATH;
            String pathUser2 = Constants.SECOND_USER_FULL_PATH;
            for (Person person : expense.getPersons()) {
                if (person.getPersonLocalID() == expense.getPersonID()) {
                    if (person.getProfilePath().equals(pathUser1)) {
                        loadGlide(ivPersonThatReceives, ivPersonThatOwes, pathUser1, pathUser2);
                        return;
                    }
                    loadGlide(ivPersonThatReceives, ivPersonThatOwes, pathUser2, pathUser1);
                }
            }
        }

        private void loadGlide(ImageView ivMain, ImageView ivSecond,
                               String userPathMain, String userPathSecond) {
            Glide.with(ivMain.getContext())
                    .load(Uri.fromFile(new File(userPathMain)))
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(ivMain);
            Glide.with(ivSecond.getContext())
                    .load(Uri.fromFile(new File(userPathSecond)))
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(ivSecond);
        }

        private void setActions(Expense expense) {
            ibDelete.setOnClickListener(v -> callback.onItemDelete(expense));
            ibEdit.setOnClickListener(v -> callback.onItemEdit(expense));
            ibPay.setOnClickListener(v -> callback.onItemPayment(expense));
        }


    }
}