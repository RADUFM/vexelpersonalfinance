package com.example.vexelpersonalfinance.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.example.vexelpersonalfinance.data.models.Expense;
import com.example.vexelpersonalfinance.data.models.ExpensesWithPeople;
import com.example.vexelpersonalfinance.data.repositories.ExpenseRepository;

import java.util.ArrayList;
import java.util.List;

public class MainActivityViewModel extends ViewModel implements Observer<Double> {

    private ExpenseRepository expenseRepository;

    private final MediatorLiveData<Double> firstUserDebt = new MediatorLiveData<>();

    private final MediatorLiveData<Double> secondUserDebt = new MediatorLiveData<>();

    private final MediatorLiveData<Boolean> collapseHeader = new MediatorLiveData<>();

    public MainActivityViewModel(ExpenseRepository expenseRepository) {
        this.expenseRepository = expenseRepository;

        firstUserDebt.addSource(expenseRepository.getUserDebt(), this);

        secondUserDebt.addSource(expenseRepository.getUserDebt(), this);

        collapseHeader.addSource(expenseRepository.getUserDebt(), this);
    }

    private LiveData<List<Expense>> transformExpense() {
        return Transformations.map(expenseRepository.getAllExpensesWithPeople(), input -> {
            List<Expense> expensesTemp = new ArrayList<>();
            for (ExpensesWithPeople expensesWithPeople : input) {
                Expense expense = expensesWithPeople.getExpense();
                if (expense != null) {
                    expense.setPersons(expensesWithPeople.getPeople());
                }
                expensesTemp.add(expensesWithPeople.getExpense());
            }
            return expensesTemp;
        });
    }

    LiveData<List<Expense>> getExpenses() {
        return transformExpense();
    }

    void insertExpense(Expense expense) {
        expenseRepository.insertExpense(expense);
    }

    void updateExpense(Expense expense) {
        expenseRepository.updateExpense(expense);
    }

    void payAllExpenses() {
        expenseRepository.payAllExpenses();
    }

    void deleteAllExpenses() {
        expenseRepository.deleteAllExpenses();
    }

    void deleteExpense(Expense expense) {
        expenseRepository.deleteExpense(expense);
    }

    LiveData<Double> getFirstUserDebt() {
        return firstUserDebt;
    }

    LiveData<Double> getSecondUserDebt() {
        return secondUserDebt;
    }

    MediatorLiveData<Boolean> getCollapseHeader() {
        return collapseHeader;
    }

    @Override
    public void onChanged(Double aDouble) {
        if (aDouble > 0) {
            firstUserDebt.postValue(aDouble);
        }else if(aDouble < 0) {
            secondUserDebt.postValue(Math.abs(aDouble));
        }else{
            collapseHeader.postValue(true);
        }
    }
}


