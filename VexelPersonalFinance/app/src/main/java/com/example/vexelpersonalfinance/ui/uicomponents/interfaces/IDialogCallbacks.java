package com.example.vexelpersonalfinance.ui.uicomponents.interfaces;

import com.example.vexelpersonalfinance.data.models.Expense;

public interface IDialogCallbacks {

    void onDialogCreateExpense(Expense expense);
    void onDialogUpdateExpense(Expense expense);

}
