package com.example.vexelpersonalfinance.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.example.vexelpersonalfinance.data.models.Expense
import com.example.vexelpersonalfinance.data.models.ExpensesWithPeople
import com.example.vexelpersonalfinance.tools.Constants

@Dao
interface ExpenseDao {

    // Get all expenses
    @Query("SELECT * from " + Constants.EXPENSE_TABLE_NAME + " ORDER BY timestamp DESC")
    fun getAllExpenses(): LiveData<List<Expense>>

    @Transaction
    @Query("SELECT * from " + Constants.EXPENSE_TABLE_NAME + " ORDER BY timestamp DESC")
    fun getAllExpensesWithPeople(): LiveData<List<ExpensesWithPeople>>

    // Totals the amount paid by first person
    @Query("SELECT SUM(amount) FROM " + Constants.EXPENSE_TABLE_NAME + " WHERE isPaid = 0 AND personID = 1")
    fun getFirstUserSum(): LiveData<Double>

    // Totals the amount paid by first person
    @Query("SELECT SUM(amount) FROM " + Constants.EXPENSE_TABLE_NAME + " WHERE isPaid = 0 AND personID = 2")
    fun getSecondUserSum(): LiveData<Double>

    // Insert expense
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertExpense(expense: Expense)

    // Update expense
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateExpense(expense: Expense)

    // Delete certain expense
    @Delete
    fun deleteExpense(expense: Expense)

    // Wipe expense table contents
    @Query("DELETE FROM " + Constants.EXPENSE_TABLE_NAME)
    fun deleteAllExpenses()

    // Update all items as paid
    @Query("UPDATE " + Constants.EXPENSE_TABLE_NAME + " SET isPaid = 1")
    fun payAllExpenses()
}