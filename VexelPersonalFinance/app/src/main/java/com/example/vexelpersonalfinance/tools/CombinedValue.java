package com.example.vexelpersonalfinance.tools;

public class CombinedValue<A, B> {
    private A first;
    private B second;

    CombinedValue(A first, B second) {
        this.first = first;
        this.second = second;
    }

    public A getFirst() {
        return first;
    }

    public B getSecond() {
        return second;
    }
}