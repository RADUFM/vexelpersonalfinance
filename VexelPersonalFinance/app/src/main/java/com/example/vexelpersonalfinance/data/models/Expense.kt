package com.example.vexelpersonalfinance.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import com.example.vexelpersonalfinance.tools.Constants

@Entity(indices = [Index("personID")],
        tableName = Constants.EXPENSE_TABLE_NAME,
        foreignKeys = [ForeignKey(entity = Person::class,
                parentColumns = ["personLocalID"],
                childColumns = ["personID"],
                onDelete = CASCADE)])
data class Expense(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "expenseID")
        var expenseID: Int?,
        @ColumnInfo(name = "expenseType")
        var expenseType: String?,
        @ColumnInfo(name = "personID")
        var personID: Int?,
        @ColumnInfo(name = "amount")
        var amount: Double?,
        @ColumnInfo(name = "description")
        var description: String?,
        @ColumnInfo(name = "timestamp")
        var timestamp: String?,
        @ColumnInfo(name = "isPaid")
        var isPaid: Int?) : Parcelable {

    constructor(expenseType: String?,
                personID: Int?,
                amount: Double?,
                description: String?,
                timestamp: String?,
                isPaid: Int?)
            : this(null, expenseType, personID, amount, description, timestamp, isPaid)

    @Ignore
    var persons: List<Person>? = null

    // Parcelable Methods

    override fun writeToParcel(p0: Parcel?, p1: Int) {}

    override fun describeContents(): Int { return 0 }

    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readInt(),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int)

    companion object CREATOR : Parcelable.Creator<Expense> {
        override fun createFromParcel(parcel: Parcel): Expense {
            return Expense(parcel)
        }
        override fun newArray(size: Int): Array<Expense?> {
            return arrayOfNulls(size)
        }
    }
}