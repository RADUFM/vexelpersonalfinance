package com.example.vexelpersonalfinance.ui.uicomponents.interfaces;

import com.example.vexelpersonalfinance.data.models.ExpenseType;

public interface IExpenseTypeAdapterCallback {

    void onExpenseTypeSelected(ExpenseType expenseType);

}
