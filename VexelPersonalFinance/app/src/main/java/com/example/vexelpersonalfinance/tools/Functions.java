package com.example.vexelpersonalfinance.tools;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IThreadCallbacks;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Functions {

    /* Gets current time from system. */
    public static String getCurrentTime() {
        return String.valueOf(System.currentTimeMillis());
    }

    /* Creates Toast. */
    public static void createToast(Context context, String toastMessage) {
        Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();
    }

    /* Creates SnackBar. */
    public static void createSnackBar(View view, String snackBarMessage){
        Snackbar.make(view, snackBarMessage, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    /* Gets a profile image file from storage folder. */
    static File getImageFile(String fileName) {
        return new File(new File(Constants.INTERNAL_MEMORY_ROOT)
                + Constants.IMAGE_STORAGE_FOLDER + fileName);
    }

    /* Checks if the file got with the previous function exists. */
    public static boolean checkIfFileExists(String fileName){
        File file = getImageFile(fileName);
        return file.exists();
    }

    /* Formats a date from milliseconds to a readable format. Formats are stored in
       Constants class. */
    public static String formatDate(long mills, String dateFormat) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter
                = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mills);
        return formatter.format(calendar.getTime());
    }

    /* Uses Dexter library to check permissions fast and without manifest interaction.
      A handler returns boolean from the Permission Alert Dialog. If true, user has accepted
      permissions. */
    public static void checkAppPermissions(View view, Activity activity,
                                           SharedPreferences sharedPreferences,
                                           IThreadCallbacks<Boolean> IThreadCallbacks) {

        android.os.Handler hook = new Handler();
        Dexter.withActivity(activity)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (sharedPreferences.getBoolean("first_run", true)) {
                            Functions.createSnackBar(view, Constants.PERMISSION_GRANTED);
                        }
                        sharedPreferences.edit().putBoolean("first_run", false).apply();

                        hook.post(() -> IThreadCallbacks.onResult(true));
                    }
                    @Override
                    public void onPermissionRationaleShouldBeShown(
                            List<PermissionRequest> permissions, PermissionToken token) {

                    }
                })
                .withErrorListener(error -> Log.e("Dexter Permission Error",
                        "There was an error: " + error.toString()))
                .check();
    }
}