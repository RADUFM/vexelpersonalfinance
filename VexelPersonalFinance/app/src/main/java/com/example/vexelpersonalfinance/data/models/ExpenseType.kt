package com.example.vexelpersonalfinance.data.models

import androidx.annotation.DrawableRes
import com.example.vexelpersonalfinance.R
import com.example.vexelpersonalfinance.tools.Constants

enum class ExpenseType constructor(var text: String, @param:DrawableRes @field:DrawableRes
var drawableBlue: Int, @param:DrawableRes @field:DrawableRes
                                           var drawableWhite: Int) {

    FOOD(Constants.EXPENSE_TYPE_FOOD, R.drawable.food_blue, R.drawable.food),
    FLOWERS(Constants.EXPENSE_TYPE_FLOWERS, R.drawable.flowers_blue, R.drawable.flowers),
    GROCERIES(Constants.EXPENSE_TYPE_GROCERIES, R.drawable.groceries_blue, R.drawable.groceries),
    HOLIDAY(Constants.EXPENSE_TYPE_HOLIDAY, R.drawable.holiday_blue, R.drawable.holiday),
    PHARMACY(Constants.EXPENSE_TYPE_PHARMACY, R.drawable.pharmacy_blue, R.drawable.pharmacy),
    BILLS(Constants.EXPENSE_TYPE_BILLS, R.drawable.bills_blue, R.drawable.bills),
    CLOTHES(Constants.EXPENSE_TYPE_CLOTHES, R.drawable.clothes_blue, R.drawable.clothes),
    TRANSPORT(Constants.EXPENSE_TYPE_TRANSPORT, R.drawable.transport_blue, R.drawable.transport),
    ITEMS(Constants.EXPENSE_TYPE_ITEMS, R.drawable.items_blue, R.drawable.items),
    OTHERS(Constants.EXPENSE_TYPE_OTHERS, R.drawable.others_blue, R.drawable.others);

    companion object {
        fun getExpenseTypeForName(name: String): ExpenseType? {
            return when (name) {
                Constants.EXPENSE_TYPE_FOOD -> FOOD
                Constants.EXPENSE_TYPE_FLOWERS -> FLOWERS
                Constants.EXPENSE_TYPE_GROCERIES -> GROCERIES
                Constants.EXPENSE_TYPE_HOLIDAY -> HOLIDAY
                Constants.EXPENSE_TYPE_PHARMACY -> PHARMACY
                Constants.EXPENSE_TYPE_BILLS -> BILLS
                Constants.EXPENSE_TYPE_CLOTHES -> CLOTHES
                Constants.EXPENSE_TYPE_TRANSPORT -> TRANSPORT
                Constants.EXPENSE_TYPE_ITEMS -> ITEMS
                Constants.EXPENSE_TYPE_OTHERS -> OTHERS
                else -> null
            }
        }
    }
}
