package com.example.vexelpersonalfinance.tools;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.WindowManager;
import com.example.vexelpersonalfinance.R;
import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IThreadCallbacks;
import java.util.Objects;

public class CustomAlertDialogBuilder {

    private Activity context;
    private String alertMessage;

    public CustomAlertDialogBuilder(){
    }

    public CustomAlertDialogBuilder setContext(Activity context) {
        this.context = context;
        return this;
    }

    public CustomAlertDialogBuilder setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
        return this;
    }

    public void build(IThreadCallbacks<Boolean> IThreadCallbacks){

        android.os.Handler hook = new Handler();

        CustomAlertDialog customAlertDialog = new CustomAlertDialog(context, alertMessage);
        customAlertDialog.setCancelable(false);
        if (customAlertDialog.getWindow()!= null) {
            Objects.requireNonNull(customAlertDialog.getWindow())
                    .setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            customAlertDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
        customAlertDialog.show();
        customAlertDialog.findViewById(R.id.btAccept)
                .setOnClickListener(view ->{
                    customAlertDialog.dismiss();
                    hook.post(()-> IThreadCallbacks.onResult(true));
                });

        customAlertDialog.findViewById(R.id.btDeny)
                .setOnClickListener(view ->{
                    customAlertDialog.dismiss();
                    hook.post(()-> IThreadCallbacks.onResult(false));
                });
    }
}