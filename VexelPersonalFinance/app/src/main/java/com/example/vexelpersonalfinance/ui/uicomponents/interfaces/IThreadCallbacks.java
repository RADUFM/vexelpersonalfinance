package com.example.vexelpersonalfinance.ui.uicomponents.interfaces;

public interface IThreadCallbacks<T> {
    void onResult (T result);
}
