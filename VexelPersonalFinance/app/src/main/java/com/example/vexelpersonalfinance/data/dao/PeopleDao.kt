package com.example.vexelpersonalfinance.data.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import com.example.vexelpersonalfinance.data.models.Person
import com.example.vexelpersonalfinance.tools.Constants

@Dao
interface PeopleDao {

    // Get all people
    @Query("SELECT * from " + Constants.PEOPLE_TABLE_NAME)
    fun getAllPeople(): LiveData<List<Person>>

    // Get people count
    @Query("SELECT COUNT(personLocalID) FROM " + Constants.PEOPLE_TABLE_NAME)
    fun getPeopleCount(): LiveData<Int>

    // Insert person
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPerson(person: Person)

    // Wipe people table contents
    @Query("DELETE FROM " + Constants.PEOPLE_TABLE_NAME)
    fun deleteAllPeople()

}
