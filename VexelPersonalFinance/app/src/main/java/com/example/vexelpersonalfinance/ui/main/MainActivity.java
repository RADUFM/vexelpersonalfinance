package com.example.vexelpersonalfinance.ui.main;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vexelpersonalfinance.R;
import com.example.vexelpersonalfinance.data.database.PersonalFinanceDatabase;
import com.example.vexelpersonalfinance.data.models.Expense;
import com.example.vexelpersonalfinance.data.models.ExpenseType;
import com.example.vexelpersonalfinance.tools.Constants;
import com.example.vexelpersonalfinance.tools.CustomAlertDialogBuilder;
import com.example.vexelpersonalfinance.tools.ViewModelFactory;
import com.example.vexelpersonalfinance.ui.expensedialog.ExpenseDialogFragment;
import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IDialogCallbacks;
import com.example.vexelpersonalfinance.ui.uicomponents.interfaces.IItemCallbacks;

import java.io.File;

public class MainActivity extends AppCompatActivity implements IItemCallbacks, IDialogCallbacks {

    private Button btPayAll;
    private RecyclerView rvExpenses;
    private LinearLayout llDebtContainer;
    private ExpenseAdapter expenseAdapter;
    private TextView tvRecycleViewHeader, tvAmountDue;
    private ImageView ivPersonThatReceives, ivPersonThatOwes;
    private ImageButton btCreateExpense, btDeleteAllExpenses;

    private MainActivityViewModel viewModel;

    /* IItemCallbacks Interface Methods - When clicking on the buttons from each Expense Recycle
     * View item*/
    @Override
    public void onItemEdit(Expense expense) {
        ExpenseDialogFragment expenseDialogFragment = ExpenseDialogFragment.newInstance(expense);
        expenseDialogFragment.show(getSupportFragmentManager(), "Update Expense");
    }

    @Override
    public void onItemDelete(Expense expense) {
        deleteExpense(expense);
    }

    @Override
    public void onItemPayment(Expense expense) {
        payExpense(expense);
    }

    /* IDialogCallbacks Methods - When creating or updating a payment from dialog Fragment, it
     * sends the expense to the activity because the database operations are performed from
     * that class instead of here.*/
    @Override
    public void onDialogCreateExpense(Expense expense) {
        createExpense(ExpenseType.Companion.getExpenseTypeForName(expense.getExpenseType()),
                expense.getPersonID(), expense.getAmount(), expense.getDescription(),
                expense.getTimestamp(), expense.isPaid());
    }

    @Override
    public void onDialogUpdateExpense(Expense expense) {
        updateExpense(expense.getExpenseID(), ExpenseType.Companion.getExpenseTypeForName(expense.getExpenseType()),
                expense.getPersonID(), expense.getAmount(), expense.getDescription(),
                expense.getTimestamp(), expense.isPaid());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.MainActivityTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this, new ViewModelFactory(PersonalFinanceDatabase.Companion.getDatabase(this))).get(MainActivityViewModel.class);

        getViewReferences();
        configView();
        observe();
        setControls();
    }

    private void getViewReferences() {
        tvRecycleViewHeader = findViewById(R.id.tvRecycleViewHeader);
        btPayAll = findViewById(R.id.btPayAll);
        btCreateExpense = findViewById(R.id.btCreateExpense);
        btDeleteAllExpenses = findViewById(R.id.btDeleteAllExpenses);
        rvExpenses = findViewById(R.id.rvExpense);
        tvAmountDue = findViewById(R.id.tvAmountDue);
        ivPersonThatReceives = findViewById(R.id.ivPersonThatReceives);
        ivPersonThatOwes = findViewById(R.id.ivPersonThatOwes);
        llDebtContainer = findViewById(R.id.llDebtContainer);
    }

    private void configView() {
        expenseAdapter = new ExpenseAdapter(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rvExpenses.setLayoutManager(layoutManager);
        rvExpenses.setItemAnimator(new DefaultItemAnimator());
        rvExpenses.setAdapter(expenseAdapter);
    }

    private void observe() {
        viewModel.getExpenses().observe(this, expenses -> {
            expenseAdapter.setData(expenses);
            changeHeaderIfListEmpty(expenses.size());
            llDebtContainer.setVisibility(View.VISIBLE);
        });

        viewModel.getFirstUserDebt().observe(this, debt -> {
            tvAmountDue.setText(String.valueOf(debt));
            loadGlideImages(Constants.SECOND_USER_FULL_PATH, Constants.FIRST_USER_FULL_PATH);
            llDebtContainer.setVisibility(View.VISIBLE);
        });

        viewModel.getSecondUserDebt().observe(this, debt -> {
            tvAmountDue.setText(String.valueOf(debt));
            loadGlideImages(Constants.FIRST_USER_FULL_PATH, Constants.SECOND_USER_FULL_PATH);
            llDebtContainer.setVisibility(View.VISIBLE);
        });

        viewModel.getCollapseHeader().observe(this, collapse -> {
            llDebtContainer.setVisibility(View.GONE);
        });
    }

    public void loadGlideImages(String firstFilePath, String secondFilePath) {
        Glide.with(this)
                .load(Uri.fromFile(new File(firstFilePath)))
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(ivPersonThatOwes);
        Glide.with(this)
                .load(Uri.fromFile(new File(secondFilePath)))
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(ivPersonThatReceives);
    }

    /* Changes the header text according to the absence or presence of items inside the
    Expense List */
    private void changeHeaderIfListEmpty(int counter) {
        if (counter == 0) {
            tvRecycleViewHeader.setText(R.string.NO_EXPENSES);
            llDebtContainer.setVisibility(View.GONE);
            return;
        }
        tvRecycleViewHeader.setText(R.string.PREVIOUS_PAYMENT);
    }

    private void setControls() {
        btPayAll.setOnClickListener(view -> payAll());
        btCreateExpense.setOnClickListener(view -> {
            ExpenseDialogFragment expenseDialogFragment
                    = ExpenseDialogFragment.newInstance(null);
            expenseDialogFragment.show(getSupportFragmentManager(), "Create Expense");
        });
        btDeleteAllExpenses.setOnClickListener(view -> deleteAllExpenses());
    }

    /* Database Operations */
    public void createExpense(ExpenseType expenseType, int personID, Double amount,
                              String description, String timestamp, int isPaid) {
        viewModel.insertExpense(new Expense(expenseType.getText(), personID, amount, description,
                timestamp, isPaid));
    }

    public void updateExpense(int expenseID, ExpenseType expenseType, int personID, Double amount,
                              String description, String timestamp, int isPaid) {
        viewModel.updateExpense(new Expense(expenseID, expenseType.getText(), personID, amount,
                description, timestamp, isPaid));
    }

    private void payAll() {
        new CustomAlertDialogBuilder()
                .setContext(MainActivity.this)
                .setAlertMessage(getString(R.string.ALERT_PAY_ALL))
                .build(okPressed -> {
                    if (okPressed) {
                        viewModel.payAllExpenses();
                    }
                });
    }

    private void payExpense(Expense expense) {
        new CustomAlertDialogBuilder()
                .setContext(MainActivity.this)
                .setAlertMessage(getString(R.string.ALERT_PAY))
                .build(okPressed -> {
                    if (okPressed)
                        updateExpense(expense.getExpenseID(), ExpenseType.Companion.getExpenseTypeForName(expense.getExpenseType()),
                                expense.getPersonID(), expense.getAmount(), expense.getDescription(),
                                expense.getTimestamp(), 1);
                });
    }

    private void deleteExpense(Expense expense) {
        new CustomAlertDialogBuilder()
                .setContext(this)
                .setAlertMessage(getString(R.string.ALERT_DELETE))
                .build(result -> {
                    viewModel.deleteExpense(expense);
                });
    }

    private void deleteAllExpenses() {
        new CustomAlertDialogBuilder()
                .setContext(this)
                .setAlertMessage(getString(R.string.ALERT_DELETE_ALL))
                .build(result -> {
                    if (result) {
                        viewModel.deleteAllExpenses();
                    }
                });
    }
}