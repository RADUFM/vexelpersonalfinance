package com.example.vexelpersonalfinance.data.repositories;

import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import com.example.vexelpersonalfinance.data.dao.PeopleDao;
import com.example.vexelpersonalfinance.data.models.Person;
import java.util.List;

public class PeopleRepository {

    private PeopleDao peopleDao;

    public PeopleRepository(PeopleDao peopleDao) {
        this.peopleDao = peopleDao;
    }

    public LiveData<List<Person>> getAllPeople() {
        return peopleDao.getAllPeople();
    }

    public void insertPerson(Person person) {
        new insertPersonAsync(peopleDao).execute(person);
    }

    public void deleteAllPeople() {
        new deleteAllPeopleAsync(peopleDao).execute();
    }

    private static class insertPersonAsync extends AsyncTask<Person, Void, Void> {
        private PeopleDao personalFinanceDao;

        insertPersonAsync(PeopleDao dao) {
            personalFinanceDao = dao;
        }

        @Override
        protected Void doInBackground(final Person... params) {
            personalFinanceDao.insertPerson(params[0]);
            return null;
        }
    }

    private static class deleteAllPeopleAsync extends AsyncTask<Void, Void, Void> {
        private PeopleDao personalFinanceDao;

        deleteAllPeopleAsync(PeopleDao dao) {
            personalFinanceDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            personalFinanceDao.deleteAllPeople();
            return null;
        }
    }
}