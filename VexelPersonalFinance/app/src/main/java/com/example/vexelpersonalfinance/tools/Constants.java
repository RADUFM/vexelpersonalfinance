package com.example.vexelpersonalfinance.tools;

import android.os.Environment;

public class Constants {

    public static final String FORMAT_YYY_MMM_d_HH_mm = "yyy MMM d, HH:mm";

    public static final String EXPENSE_TABLE_NAME = "EXPENSE_TABLE";
    public static final String PEOPLE_TABLE_NAME = "PEOPLE_TABLE";

    public static final String EXPENSE_TYPE_FOOD = "FOOD";
    public static final String EXPENSE_TYPE_FLOWERS = "FLOWERS";
    public static final String EXPENSE_TYPE_GROCERIES = "GROCERIES";
    public static final String EXPENSE_TYPE_HOLIDAY = "HOLIDAY";
    public static final String EXPENSE_TYPE_PHARMACY = "PHARMACY";
    public static final String EXPENSE_TYPE_BILLS = "BILLS";
    public static final String EXPENSE_TYPE_CLOTHES = "CLOTHES";
    public static final String EXPENSE_TYPE_TRANSPORT = "TRANSPORT";
    public static final String EXPENSE_TYPE_ITEMS = "ITEMS";
    public static final String EXPENSE_TYPE_OTHERS = "OTHERS";

    public static final String EMPTY_AMOUNT_ET = "Please add your expense amount!";
    public static final String EMPTY_EXPENSE_TYPE_RV = "Please select your expense type!";
    public static final String EMPTY_PERSON_RV = "Please select the person who paid!";

    public static final String PROFILE_EMPTY_NAMES = "Please add a name for each user!";
    public static final String PROFILE_NO_PICTURES = "Please add a profile picture for each user!";

    public static final String BUNDLE_EXPENSE_KEY = "CREATE_UPDATE_EXPENSE_ID_KEY";
    public static final String BUNDLE_EXPENSE_POS = "UPDATE_EXPENSE_POSITION";

    public static final String IMAGE_STORAGE_FOLDER = "/com.expense.manager/";
    public static final String FIRST_USER_FILE_NAME = "profilePicUser1.jpg";
    public static final String SECOND_USER_FILE_NAME = "profilePicUser2.jpg";

    static final String INTERNAL_MEMORY_ROOT =
            Environment.getExternalStorageDirectory().toString();

    public static final String FIRST_USER_FULL_PATH =
            Environment.getExternalStorageDirectory().toString()
            + IMAGE_STORAGE_FOLDER + FIRST_USER_FILE_NAME;

    public static final String SECOND_USER_FULL_PATH =
            Environment.getExternalStorageDirectory().toString()
            + IMAGE_STORAGE_FOLDER + SECOND_USER_FILE_NAME;

    public static final int REQUEST_CODE_USER_1 = 55888;
    public static final int REQUEST_CODE_USER_2 = 55889;

    static final String PERMISSION_GRANTED = "Permission Granted";
    public static final String PERMISSION_DENIED = "Permission Denied";
}